package jp.nurinubi.edu.nuts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import jp.nurinubi.edu.nuts.dto.UserDto;
import jp.nurinubi.edu.nuts.entity.User;
import jp.nurinubi.edu.nuts.repository.UserRepository;

@Controller
public class UserEntryController {
	
	@RequestMapping(path = "/userEntry")
	public String index() {
		return "/userEntry/index.html";
	}
	
	
	@RequestMapping(path = "/confirm")
	public String confirm(@ModelAttribute("userDto") UserDto userDto) {
		return "/userEntry/confirm.html";
	}
	@Autowired
	UserRepository repository;
	
	@RequestMapping(path = "/register")
	public String register(@ModelAttribute("userDto") UserDto dto) {
		User user = new User();
		user.setUserId(dto.getUserId());
		user.setUserPass(dto.getUserPass());
		user.setUserName(dto.getUserName());
		user.setEmail(dto.getEmail());
		user.setPhone(dto.getPhone());
		repository.save(user);
		
		return "/userEntry/result.html";
	}
}
