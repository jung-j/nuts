package jp.nurinubi.edu.nuts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.nurinubi.edu.nuts.repository.UserRepository;

@Controller
public class UserSearchController {
	@Autowired
	UserRepository repository;
	
	@RequestMapping(path = "/userSearch")
	public String index(Model model) {
		model.addAttribute("users", repository.findAll());
		
		return "userSearch/index.html";
	}
}
