package jp.nurinubi.edu.nuts.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestSearchController {
	
	@RequestMapping(path = "/testSearch")
	public String index() {
		return "/testSearch/index.html";
		
	}
}
