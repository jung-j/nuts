package jp.nurinubi.edu.nuts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jp.nurinubi.edu.nuts.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{

}
